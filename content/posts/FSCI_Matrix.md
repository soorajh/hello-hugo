---
title: "FSCI Matrix Group"
date: 2020-02-06T19:41:17+05:30
draft: false
author: "MA"
---

## The lockdown days

Exploring the FOSS side of the world I came across the matrix ecosystem. A Federated system with servers run by communities and people around the globe and not owned by any particular company. With the freedom to even self host your own server, I began exploring more of Matrix.

I joined the FSCI (Free Software Community of India) from the matrix public groups. A very interesting group with a lot of people volunteering their time and energy to promote and work for open source and free software.

***To promote freedom in the digital world.***

During this time FSCI was starting an online camp to teach and guide the new people who were interested to volunteer and be part of the community.

[The road to Free Software Camp](/posts/fscamp)
