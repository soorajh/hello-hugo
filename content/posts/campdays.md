---
title: "Camp Days"
date: 2021-04-02T21:41:17+05:30
draft: "false"
author: "MA"
#slug: "chakka"
---
Camp was real fun with activities and interactions with the mentors and the participants and even guest speakers, at times. Everyone in the coomunity were volunteering their time and energy for just one cause. _Freedom_. I got to know more about the free Software philosophy from the camp and even more from the discussions in the matrix group.

All these reminded me of some ideas I had a year ago, but more on it some other day.

There were even some movie marathon sessions during the camp. We all met online to watch movies made using _Blender_ together.

[Some of the must watch movies you can find here.](https://videos.fsci.in/)

In one of the session we had a chance to talk to some guest speakers from the free software community across the globe. It was a very interesting session, getting to know more about their contributions to the society and more importantly knowing why they do what they were doing.

They self host and provide a lot of amazing services to the public for ***free***. Free video conferencing tools, E2EE collaborative project tools to even cloud servers for data storage for the public are a few examples of their services.

Some people who I remember from that session and the services they provide:

[Amolith who runs Nixnet Services](https://nixnet.services)

[Muppeth who runs the infamous Disroot Services](https://disroot.org)

[& Aaron who runs Cryptpad service ](https://cryptpad.fr/)

They run these services on crowd funding and mostly out of their own pockets. _And I must agree, hosting these services are not easy and pocket friendly._

Moreover, these services do not compromise on user data and promises privacy to the users unlike the corporate ones who are hungry for user data.

Hopefully, more people would become aware of the free software philosophy and the beautiful community around it.

 _Respecting user's privacy and freedom._

There are many more public run services out there _run for the people by the people_ awaiting to be explored.

One main free software I decided to explore : ***Linux***.

[_Goodbye to windows_]()
