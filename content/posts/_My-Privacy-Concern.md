---
title: "My Privacy Concern"
date: 2021-03-03T19:45:17+05:35
draft: "true"
---

People ask me why i am sooo concerned about my data.

Should i not be concerned?

Maybe if i was living in new London of the brave new world!

I guess even in such a world I would be more inclined towards the savage side of it. To be in the open and free. I hope everybodys savage side is in someway or the other inclined to live in a free and open world     

{*Please watch The brave New World on Netflix if you have not*}

Maybe its the ***chit*** side of our mind, trying to live on the edge. To try and feel whole or complete by embracing chaos.

But the other part of our mind needs its privacy.

One of the major reasons i joined the FSCI Matrix group was for my concern for the digital privacy. Maybe to try and do something for it. To keep my digital rights my digital freedom.

Imagine your own home bugged with surveillance systems and being watched 24*7 and judged for your choices. And say you have no choice to turn this system off even for a second and even if you figured out to turn this system off you will be judged to go against the system for turning the system off. So you accept the system so as to not be judged as a villain.

Maybe i think too much maybe not. But i feel thats how I am and we all are programmed internally. Afterall we are conscious beings.Or are we not?

Joining the camp I understood more about the importance of digital freedom and our digital rights in this digital era. I felt happy that there were more people who were not only concerned but even worked towards keeping their rights as it is supposed to be. I got introduced to the world of free softwares.Thanks to FSCI.
Though there is still a lot more to learn i have started taking my baby steps.
