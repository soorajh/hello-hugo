---
title: "FSCamp"
date: 2021-03-02T21:41:17+05:30
draft: false
author: "MA"
---
Registering for the fscamp was one of the exciting decisions I took during the lockdown days. There were so many participants from across India in the camp. Mostly college students doing their engineering in computer science. I was excited to be part of the camp and at the same time wondering if I will be able to keep up with the activities along with my office work.

_Nevertheless, better late than never_

[Initial days in the camp](/posts/campdays)
